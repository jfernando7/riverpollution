/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React,{useState} from 'react';
 import { Text, 
  StyleSheet, 
  View,
  Image,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  ScrollView
} from 'react-native';
 import Formulario from './Formularios/Login';
 import Registrar from './Formularios/Registrar';
 import Menu from './Formularios/Menu';


 const App = () => { 
  
  const [mostrarform, guardarMostrarForm] = useState(false);
  const [mostrarform2, guardarMostrarForm2] = useState(false);
  const [mostrarform3, guardarMostrarForm3] = useState(false);

    //definicion de state para usuarios
    const [usuarios, setUsuarios] = useState([
     { email:'admin',password:'adimin'}

    ]);

    
    //definicion de state para registrar usuarios
    const [registrarUsuario, setRegistrarUsuario] = useState([]);

    //cerrar teclado
    const cerrarTeclado = () => {
      Keyboard.dismiss();
    }
  
   return (
    <ScrollView style={styles.formulario}>
    <TouchableWithoutFeedback onPress={() => cerrarTeclado()}>
     <View style={styles.contenedor}>
     {!mostrarform ? (
         <>
       <View style={styles.containerimagen}>
       <Image
                style={styles.imagen}
                 source={require('./android/imagenes/river.png')}
            /> 
       </View>
       <View>
        <Text style={styles.titulo}>Bienvenido </Text>
        </View>
       <View>
        <Text style={styles.label}>¡Hola!, que gusto verte de nuevo </Text>
        </View>
        
       <Formulario 
          usuarios={usuarios}
          setUsuarios={setUsuarios}
          mostrarform={mostrarform}
          guardarMostrarForm={guardarMostrarForm}
          mostrarform={mostrarform}
          guardarMostrarForm2={guardarMostrarForm2}
          mostrarform2={mostrarform2}
       />
       </>
       ):(<View></View>)}

       {mostrarform ? (
         <>
       <Registrar
          registrarUsuario={registrarUsuario}
          setRegistrarUsuario={setRegistrarUsuario}
          guardarMostrarForm={guardarMostrarForm}
          mostrarform={mostrarform}
          guardarMostrarForm3={guardarMostrarForm3}
          mostrarform3={mostrarform3}
       
       />
       </>
       ):(<View></View>)}
     

       {mostrarform2 ? (
         <>
          <View style={styles.containerimagen}>
       <Image
                style={styles.imagen}
                 source={require('./android/imagenes/river.png')}
            /> 
       </View>
       <Menu
       guardarMostrarForm={guardarMostrarForm}
       mostrarform={mostrarform}
       guardarMostrarForm2={guardarMostrarForm2}
       mostrarform2={mostrarform2}
       />
        </>
       ):(<View></View>)}
     </View>
     </TouchableWithoutFeedback>
     </ScrollView>
   );
 };
 
 //Hoja de stilos de esta sección
 
 const styles = StyleSheet.create({
   contenedor: {
     backgroundColor:'#e6e6fa',
     flex:1
   },
 
   titulo:{
     color: 'black',
     marginTop:10,
     marginBottom:1,
     fontSize:30, 
     fontWeight: 'bold',
     textAlign:'center'
 
   },
   imagen:{
    width:330,
    height:200,
    marginTop:30,
    
},

  containerimagen:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  label:{
    color:'black',
    fontWeight:'bold',
    fontSize:18,
    marginTop:10,
    fontWeight: 'bold',
    textAlign:'center'


  },
  label2:{

    fontWeight:'bold',
    fontSize:30,
    marginTop:30,
    justifyContent: 'center',
    alignItems:'center'

  }
 })
 
 export default App;


