import React,{useState} from 'react';
 import { Text, 
  StyleSheet, 
  View,
  Alert,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  TextInput,
  Animated,
  BackAndroid
} from 'react-native';
import shortid from 'shortid';

const Menu = (guardarMostrarForm,mostrarform,guardarMostrarForm2,mostrarform2) => { 

  const mostrarFormulario4 = () => {
    console.log('mostrar u ocultar formulario menu');
    guardarMostrarForm2(!mostrarform2);
};

    //useState
    const [animacionBoton] = useState(new Animated.Value(1));

    //funciones 
    const animacionEntrada=()=>{
        Animated.spring(animacionBoton,{
            toValue:.75,
            useNativeDriver:true           
        }).start();
    };

    const animacionSalida=()=>{
        Animated.spring(animacionBoton,{
            toValue:1,
            useNativeDriver:true,
            friccion:4,
            tension:90,
        }).start();
    };

    const estiloAnimacion = {
        transform:[{scale:animacionBoton}]
    };

    const datosPersonales= ()=>{

      mostrarFormulario4();
    }

    return (

    <>
    <View style={styles.Formulario}>
    
    <View>
          <TouchableHighlight
             onPress={() =>  datosPersonales()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>Datos personales</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() => mostrarFormulario4()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>Crear reporte</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() => mostrarFormulario4()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>Mostrar reportes</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() =>  mostrarFormulario4()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>A quien acudir</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() => mostrarFormulario4()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>Ayuda</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() => mostrarFormulario4()}
            style={styles.btn}>
            <Text style={styles.textoBtn}>Configuracion</Text>
          </TouchableHighlight>
        </View>

<View>
    <TouchableWithoutFeedback
            
            onPressIn={()=> animacionEntrada()}
            onPressOut={()=>animacionSalida()}
            onPress={() => mostrarFormulario4()}
            >

            <Animated.View style={[styles.btnBuscar, estiloAnimacion]}>
                <Text style={styles.textoBuscar}>Cerrar sesion</Text>
            </Animated.View>
     </TouchableWithoutFeedback>


</View>
</View>
    </>

 );

}

const styles = StyleSheet.create({

    Formulario:{
        backgroundColor:'#FFF',
        paddingHorizontal:20,
        paddingVertical:10,
        marginHorizontal:'5%'
    },
    input: {
      padding: 10,
      height: 50,
      backgroundColor: '#FFF',
      fontSize: 20,
      marginBottom: 20,
      textAlign: 'center',
    },
    btnBuscar: {
      marginTop: 50,
      backgroundColor: '#ff7f50',
      padding: 10,
      justifyContent: 'center',
    },
    textoBuscar: {
      color: '#FFF',
      textTransform: 'uppercase',
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize:18
    },
    imagen:{
        width:330,
        height:200,
        marginTop:30,
        
    },
    btn:{
        padding:15,
        backgroundColor:"red",
        marginVertical:5
      },
    
      textoBtn: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
      }
  });

  export default Menu;

