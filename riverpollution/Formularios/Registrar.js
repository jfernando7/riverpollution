import React,{useState} from 'react';
 import { Text, 
  StyleSheet, 
  View,
  Alert,
  Keyboard,
  TouchableHighlight,
  TouchableWithoutFeedback,
  TextInput
} from 'react-native';
import shortid from 'shortid';


const Registrar = ({registrarUsuario,setRegistrarUsuario,guardarMostrarForm,mostrarform}) => { 

   
  const [usuarios2, setUsuarios2] = useState([]);

        //UseState para capturar Email, Password
    
        const [email2,guardarEmail2] = useState('');
        const [password2,guardarPassword2] = useState('');
        const [nombres,guardarNombres] = useState('');
        const [apellidos,guardarApellidos] = useState('');
        const [identificacion,guardarIdentificacion] = useState('');
        const [ciudad,guardarCiudad] = useState('');
        const [direccion,guardarDireccion] = useState('');
    
        //formulario mostrar o ocultar
      const mostrarFormulario2 = () => {
      console.log('mostrar u ocultar formulario registrar');
      guardarMostrarForm(!mostrarform);
    };

        //Metodo para Continuar sesion
        const continuar = () => {
            console.log('guardando desde formulario');
        
            //Validar campos llenos
        
            if (
              email2.trim() === '' ||
              password2.trim() === ''||
              nombres.trim() === '' ||
              apellidos.trim() === '' ||
              identificacion.trim() === '' ||
              ciudad.trim() === '' ||
              direccion.trim() === '' 
            ) {
              //true, es decir algo falló -> algún campo vacio
              console.log('Algo falló, los campos están vacios');
              mostrarAlertaregistrar();
              return;
            }
        
            const registrarUsuarios = {email2, password2, nombres, apellidos, identificacion, ciudad, direccion};
            registrarUsuarios.id = shortid.generate();
            const nuevoregistro = [...registrarUsuario, registrarUsuarios];
            setRegistrarUsuario(nuevoregistro);
            guardarMostrarForm(false);
            console.log({registrarUsuario});
                              
          };
        
            const mostrarAlertaregistrar = () => {
             Alert.alert(
                '¡Error', //Titulo
                'Todos los campos son obligatorios', //mensaje
                [
                    {text: 'OK', onPress: () => console.log('Presiono el ok')},
                    {text: 'cancelar'},
                ],
                );
            };

return(
    <>
    
    <View style={styles.Formulario}>
    <View style={styles.contenedor}>
    <View>
    <Text style={styles.titulo}>Registro de nuevo usuario</Text>
    </View>
</View>
    <View>
        <Text style={styles.label2}>Email:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarEmail2(texto)}               
        />
    </View>
    <View>
        <Text style={styles.label2}>Password:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarPassword2(texto)}
        />
    </View>
    <View>
        <Text style={styles.label2}>Nombres:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarNombres(texto)}               
        />
    </View>
    <View>
        <Text style={styles.label2}>Apellidos:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarApellidos(texto)}               
        />
    </View>
    <View>
        <Text style={styles.label2}>Identificación:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarIdentificacion(texto)} 
            keyboardType="phone-pad"              
        />
    </View>
    <View>
        <Text style={styles.label2}>Ciudad:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarCiudad(texto)}               
        />
    </View>
    <View>
        <Text style={styles.label2}>Direccion:</Text>
        <TextInput
            style={styles.input}
            onChangeText={texto => guardarDireccion(texto)}  
                         
        />
    </View>


    <View>
    <View>
      <TouchableHighlight
        onPress={() => continuar()}
        style={styles.btnContinuar}>
        <Text style={styles.textoContinuar}>Continuar</Text>
      </TouchableHighlight>
    </View>

    <View>
      <TouchableHighlight
        onPress={() =>  mostrarFormulario2()}
        
        
        style={styles.btnCancelar}>
        <Text style={styles.textoCancelar}>Cancelar</Text>
        
      </TouchableHighlight>
    </View>
    </View>
    </View>
    </>
);
      
}




const styles = StyleSheet.create({
    contenedor: {
      backgroundColor:'#e6e6fa',
      flex:1
    },
  
    titulo:{
      color: 'black',
      marginTop:10,
      marginBottom:1,
      fontSize:30, 
      fontWeight: 'bold',
      textAlign:'center'
    },

    Formulario:{
        backgroundColor:'#FFF',
        paddingHorizontal:20,
        paddingVertical:10,
        marginHorizontal:'5%'
    },
    label:{

        fontWeight:'bold',
        fontSize:18,
        marginTop:20

    },
    label2:{
        backgroundColor:'#FFF',
        color:'red',
        fontWeight:'bold',
        fontSize:18,
        marginTop:20

    },
    input:{
        color:'black',
        marginTop:5,
        height:35,
        borderColor:'#e1e1e1',
        borderWidth:1,
        borderStyle:'solid'

    },
    btnContinuar:{
        marginTop:5,
        padding:15,
        backgroundColor:"red",
        marginVertical:10
      },
    
      textoContinuar: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
      },
    
      btnCancelar:{
        padding:15,
        backgroundColor:"red",
        marginVertical:5
      },
    
      textoCancelar: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
      }
    })
    export default Registrar;

    