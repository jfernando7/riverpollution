import React,{useState} from 'react';
import {Text,
    StyleSheet,
    View,TextInput,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import shortid from 'shortid';

const Formulario =({usuarios,setUsuarios,guardarMostrarForm,mostrarform,guardarMostrarForm2,mostrarform2,guardarMostrarForm3,mostrarform3})=>{


    //UseState para capturar Email, Password
    
    const [email,guardarEmail] = useState('');
    const [password,guardarPassword] = useState('');

    //formulario mostrar o ocultar
    const mostrarFormulario = () => {
      console.log('mostrar u ocultar formulario');
      guardarMostrarForm(!mostrarform);
  };

  const mostrarFormulario3 = () => {
    console.log('mostrar u ocultar formulario');
    guardarMostrarForm2(!mostrarform2);
};

    //Metodo para Inicar sesion
    const iniciarSesion = () => {
    console.log('guardando desde formulario');

    const usuario = {email, password};
    usuario.id = shortid.generate();
    const nuevousuario = [...usuarios,usuario];
    setUsuarios(nuevousuario);
    console.log({usuarios});
    //Validar campos llenos

    if (
      email.trim() === '' || password.trim() === ''

    ) {
      //true, es decir algo falló -> algún campo vacio
      console.log('Algo falló, los campos están vacios');
      mostrarAlerta();
      return;
    }else if(usuario.password==='admin' && usuario.email==='admin'){
      mostrarAlerta2();
      mostrarFormulario3();
      console.log('usuario correcto');
    }else{
      mostrarAlerta3();

    }
   
  };

    const mostrarAlerta = () => {
     Alert.alert(
        '¡Error', //Titulo
        'los campos de Email y password son obligatorios', //mensaje
        [
            {text: 'OK', onPress: () => console.log('Presiono el ok')},
            {text: 'cancelar'},
        ],
        );
    };

    const mostrarAlerta2 = () => {
      Alert.alert(
         '¡Error', //Titulo
         'Email y password correctos', //mensaje
         [
             {text: 'OK', onPress: () => console.log('Presiono el ok')},
             {text: 'cancelar'},
         ],
         );
     };

     const mostrarAlerta3 = () => {
      Alert.alert(
         '¡Error', //Titulo
         'Email y password incorrectos', //mensaje
         [
             {text: 'OK', onPress: () => console.log('Presiono el ok')},
             {text: 'cancelar'},
         ],
         );
     };


    return(
        <>
        <View style={styles.Formulario}>
        <View>
            <Text style={styles.label2}>Email:</Text>
            <TextInput
                style={styles.input}
                onChangeText={texto => guardarEmail(texto)}               
            />
        </View>
        <View>
            <Text style={styles.label2}>Password:</Text>
            <TextInput
                style={styles.input}
                onChangeText={texto => guardarPassword(texto)}
            />
        </View>

        <View>
          <TouchableHighlight
            onPress={() => iniciarSesion()}
            style={styles.btnIniciar}>
            <Text style={styles.textoIniciar}>Iniciar sesion</Text>
          </TouchableHighlight>
        </View>

        <View>
          <TouchableHighlight
            onPress={() => mostrarFormulario()}
            style={styles.btnRegistar}>
            <Text style={styles.textoRegistrar}>Registarse</Text>
          </TouchableHighlight>
        </View>

        </View>
        </>
    );
}

const styles =StyleSheet.create({

    Formulario:{
        backgroundColor:'#FFF',
        paddingHorizontal:20,
        paddingVertical:10,
        marginHorizontal:'5%'
    },
    label:{

        fontWeight:'bold',
        fontSize:18,
        marginTop:20

    },
    label2:{
        backgroundColor:'#FFF',
        color:'red',
        fontWeight:'bold',
        fontSize:18,
        marginTop:20

    },
    input:{
      color:'black',
        marginTop:5,
        height:35,
        borderColor:'#e1e1e1',
        borderWidth:1,
        borderStyle:'solid'

    },
    btnIniciar:{
        marginTop:5,
        padding:15,
        backgroundColor:"red",
        marginVertical:10
      },
    
      textoIniciar: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
      },
    
      btnRegistar:{
        padding:15,
        backgroundColor:"red",
        marginVertical:5
      },
    
      textoRegistrar: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
      }
    
})

export default Formulario;
